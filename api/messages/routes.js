const messagesRouter = require('express').Router()
const auth = require('../../misc/').auth
const callback = require('../../misc/').routerCallback
const messages = require('./')
const multer = require("multer")
const storagePath = '../data/smackImages/'

let upload = multer({ dest: storagePath })
const type = upload.single('recfile')

module.exports = router => {
    
    messagesRouter.get('/', auth.isLoggedIn, (req, res) => {
        messages.readAll(callback(res), req.hostname == 'localhost')
    })

    messagesRouter.post('/', auth.isLoggedIn, (req, res) => {
        req.body.user = req.user._id
        messages.create(req.body, req.hostname == 'localhost', callback(res))
    })

    messagesRouter.delete('/:fileName', auth.isLoggedIn, (req, res) => {
        messages.deleteMessage(callback(res), req.params.fileName, req.user._id)
    })
    
    messagesRouter.post('/uploadImage', auth.isLoggedIn, type, (req, res) => {
        messages.uploadImage(req, req.hostname == 'localhost', storagePath, callback(res))
    })

    router.use('/messages', messagesRouter)
}