const fs = require('fs')
const path = require('path')
const jsonfile = require('jsonfile')

const messagesPath = path.normalize(__dirname + '/../../../data/messages')

function readJsonFile(filename, contents) {
    return new Promise((resolve, reject) => {

        const fullFilePath = path.normalize(messagesPath + '/' + filename)

        jsonfile.readFile(fullFilePath, (err, obj) => {
            
            if(err) {
                reject()
            }else{
                contents[filename] = obj
                resolve()
            }
        })
    })
}

function returnReadJsonFile(filename, contents) {
    return () => {
        return readJsonFile(filename, contents)
    }
}

function chainer(filenames, contents, func) {

    let chain = Promise.resolve()

    filenames.forEach(filename => {
        chain = chain.then(func(filename, contents))
    })

    return chain
}

function getNewFileName() {

    const dat = new Date()
    const d = dat.getDate()
    const m = dat.getMonth()
    const y = dat.getFullYear()
    const t = dat.getTime()

    return y + '-' + m + '-' + d + '-' + t + '.json'
}

function converntFileNameToDate(fn) {

    fn = fn.replace('.json', '')
    let a = fn.split('-')
    let d = new Date(a[0], a[1], a[2], 0,0,0,0)

    d.setTime(a[3])

    return d
}

module.exports = {
    
    readAll: (callback, local) => {

        fs.readdir(messagesPath, function(err, filenames) {

            if(err) {
                return
            }

            let contents = {}

            chainer(filenames, contents, returnReadJsonFile).then(() => {

                // console.log(filenames)
                // console.log(contents)
                // console.log(returnReadJsonFile)
                
                let ret = []

                for(let prop in contents) {

                    const msg = {
                        file: prop,
                        id: converntFileNameToDate(prop),
                        user: contents[prop].user,
                        text: contents[prop].text
                    }

                    if(contents[prop].image) {
                        const afilePrefix = local ? 'http://localhost:1234' : ''
                        msg.image = afilePrefix + contents[prop].image
                    }

                    ret.push(msg)
                }

                ret.sort((a, b) => {
                    return a.id.getTime() - b.id.getTime()
                })

                callback.success(ret)
            })
            .catch(err => {
                callback.error(err)
                console.log(`OH CRAP ${err}`)
            })
        })
    },


    create: (body, local, callback) => {

        const fn = getNewFileName()
        const fullFilePath = path.normalize(messagesPath + '/' + fn)

        jsonfile.writeFile(fullFilePath, body, err => {
            
            if(err) {
                return callback.error(err)
            }

            const newMsg = {
                file: fn,
                id: converntFileNameToDate(fn),
                user: body.user,
                text: body.text
            }

            if(body.image) {
                const afilePrefix = local ? 'http://localhost:1234' : ''
                newMsg.image = afilePrefix + body.image
            }

            callback.success(newMsg)
        })
    },

    deleteMessage : (callback, filename, userId) => {

        const fullFilePath = path.normalize(messagesPath + '/' + filename)

        jsonfile.readFile(fullFilePath, (err, obj) => {
            
            if(err) {
                callback.error(err)
            }else{
                
                if(obj.user == String(userId)) {
                    
                    fs.unlink(fullFilePath, (err) => {

                        if (err) {
                            return callback.error(err)
                        }
                        
                        callback.success({deleted: true})
                    })

                }else{
                    callback.error('Invalid permission!')
                }
            }
        })
    },

    uploadImage : (req, local, storagePath, callback) => {

        const playerId = req.user._id
        const tempPath = req.file.path
        const filenameArray = req.file.originalname.split('.')
        const extension = filenameArray.pop().toLowerCase()
        const filename = filenameArray.join('.').replace(/ /g, '').toLowerCase()
        const d = new Date()
        const t = d.getTime()
        const targetPath = `${storagePath}${playerId}.${t}.${filename}.${extension}`
        const src = fs.createReadStream(tempPath)
        const dest = fs.createWriteStream(targetPath)

        src.pipe(dest)

        src.on('end', () => {

            fs.unlinkSync(tempPath)

            filePath = targetPath.slice(2, targetPath.length)

            callback.success({filePath})
        })

        src.on('error', (err) => {
            callback.error(err)
        })
    }

}