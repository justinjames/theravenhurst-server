const requestify = require('requestify');

const EntryMain = require('../entry');
const Entry = EntryMain.Entry;
const Game = require('./model').Game;
const Player = require('../player/model').Player;
let KnockoutMain = require('../knockout');
let Knockout = KnockoutMain.Knockout;
// let HighHandMain = require('../hand')
let HandModel = require('../hand').Hand;
const gameSchema = require('./model').gameSchema;

function getRecentGamesIdsToDeactivatePlayers() {
    const q = {
        del: false,
        s: 3,
    };

    Game.find(q)
        .select('_id')
        .sort({ date: -1 })
        .limit(5)
        .exec((err, _ids) => {
            if (err) {
                return;
            }

            const ids = _ids.map((i) => String(i._id));

            EntryMain.getPlayersFromGames(ids)
                .then((res) => {
                    const pIds = res.map((e) => String(e.p));
                    const unique = [...new Set(pIds)];

                    Player.update(
                        { _id: { $in: unique } },
                        { $set: { active: true } },
                        { multi: true }
                    ).exec((err, players) => {
                        // console.log('act', players.n)
                    });

                    Player.update(
                        { _id: { $nin: unique } },
                        { $set: { active: false } },
                        { multi: true }
                    ).exec((err, players) => {
                        // console.log('inact', players.n)
                    });
                })
                .catch((err) => {
                    console.log('err', err);
                });
        });
}

module.exports = {
    Game,
    gameSchema,

    create: (gameDate, callback) => {
        if (!gameDate) {
            return callback.error(`Missing game date.`);
        } else if (
            gameDate.day == undefined ||
            gameDate.month == undefined ||
            gameDate.year == undefined
        ) {
            return callback.error(`Invalid game date.`);
        }

        const newGame = {
            d: gameDate.day,
            m: gameDate.month,
            y: gameDate.year,
        };
        const q = Object.assign({}, newGame);

        q.del = false;

        Game.findOne(q, (err, exists) => {
            if (err) {
                return callback.error(err);
            } else if (exists) {
                return callback.error(`A game with this date already exists.`);
            }

            newGame.date = gameDate.date;

            Game.create(newGame, (err, game) => {
                if (err) {
                    return callback.error(err);
                }
                callback.success(game);
            });
        });
    },

    saveGame: (gameId, gameData, callback) => {
        if (!gameData) {
            return callback.error(`No game data.`);
        }

        Game.findByIdAndUpdate(gameId, gameData, { new: true }, (err, game) => {
            if (err) {
                return callback.error(err);
            }

            callback.success(game);
        });
    },

    /*changeDate : (gameId, newData, callback) => {

        if(!newData) {
            return callback.error(`No game data.`)
        }else if(!newData.d || !newData.m || !newData.y) {
            return callback.error(`Invalid or missing game date.`)
        }

        let q = {
            _id: { $ne : gameId },
            d: newData.day,
            m: newData.month,
            y: newData.year
        }

        Game.findOne(q, (err, exists) => {

            if(err) {
                return callback.error(err)
            }else if(exists) {
                return callback.error(`A game with this date already exists.`)
            }

            delete q._id

            Game.findByIdAndUpdate(gameId, q, {new : true}, (err, game) => {
                if(err) {
                    return callback.error(err)
                }
                callback.success(game)
            })
        })
    },*/

    /*addPlayer : (gameId, playerId, callback) => {

        Player.findById(playerId, (err, player) => {

            if(err) {
                return callback.error(err)
            }else if(!player) {
                return callback.error(`Player not found.`)
            }

            Game.findByIdAndUpdate(gameId, {$addToSet: {players: player}}, {new : true}, (err, game) => {

                if(err) {
                    return callback.error(err)
                }
                
                callback.success(game)
            })
        })
    },

    removePlayer : (gameId, playerId, callback) => {

        Player.findById(playerId, (err, player) => {

            if(err) {
                return callback.error(err)
            }else if(!player) {
                return callback.error(`Player not found.`)
            }

            Game.findByIdAndUpdate(gameId, {$pull: {players: player._id}}, {new : true}, (err, game) => {
                if(err) {
                    return callback.error(err)
                }
                callback.success(game)
            })
        })
    },*/

    getAllGameIdsForYear: (year) => {
        const q = {
            y: year,
            del: false,
            s: 3,
        };

        return Game.find(q).select('_id date').sort('date');
    },

    readAll: (year, callback) => {
        const startDate = new Date(year, 0, 1, 0, 0, 0, 0);
        const endDate = new Date(++year, 0, 1, 0, 0, 0, 0);
        const q = {
            del: false,
            date: {
                $gte: startDate,
                $lt: endDate,
            },
        };

        Game.find(q)
            .select('d m y date s n name')
            .sort({ date: -1 })
            // .skip(2)
            // .limit(2)
            .exec((err, games) => {
                if (err) {
                    return callback.error(err);
                }

                Entry.find({ g: { $in: games } })
                    .populate('p', 'first last')
                    .exec((err, entries) => {
                        if (err) {
                            return callback.error(err);
                        }

                        let retGames = [];

                        games.forEach((g) => {
                            let ret = {
                                _id: g._id,
                                date: g.date,
                                d: g.d,
                                m: g.m,
                                y: g.y,
                                s: g.s,
                            };

                            ret.entries = entries.filter((e) => {
                                return e.g.toString() == g._id.toString();
                            });

                            retGames.push(ret);
                        });

                        callback.success(retGames);
                    });
            });
    },

    readCount: (year, callback) => {
        const startDate = new Date(year, 0, 1, 0, 0, 0, 0);
        const endDate = new Date(++year, 0, 1, 0, 0, 0, 0);
        const q = {
            del: false,
            date: {
                $gte: startDate,
                $lt: endDate,
            },
        };

        Game.count(q).exec((err, count) => {
            if (err) {
                return callback.error(err);
            }

            callback.success({ count });
        });
    },

    readLatest: (local, callback) => {
        const q = {
            del: false,
            s: 3,
        };
        const gameSelect = 'd m y date s n name';
        const playerSelect = 'first last nick say afile';

        Game.find(q)
            .select(gameSelect)
            .sort({ date: -1 })
            .limit(1)
            .exec((err, games) => {
                if (err || !games) {
                    return callback.error(err);
                }

                if (games.length == 0) {
                    return callback.success({});
                }

                const game = games[0];

                Entry.find({ g: game })
                    .populate('p', playerSelect)
                    .exec((err, gameEntries) => {
                        if (err) {
                            return callback.error(err);
                        }

                        const entries = [];

                        gameEntries.forEach((ge) => {
                            entries.push({
                                p: {
                                    _id: ge.p._id,
                                    first: ge.p.first,
                                    last: ge.p.last,
                                    afile: ge.p.afile,
                                    nick: ge.p.nick,
                                    say: ge.p.say,
                                },
                                b: ge.b,
                                c: ge.c,
                                ks: 0,
                                q: ge.q,
                                pt: ge.pt,
                                hh: ge.hh,
                            });
                        });

                        const retGame = {
                            // _id: game._id,
                            // entries
                        };

                        // const gameProps = gameSelect.split(' ')

                        // gameProps.forEach(prop => {
                        //     retGame[prop] = game[prop]
                        // })

                        entries.forEach((e) => {
                            const afilePrefix = local ? 'http://localhost:1234' : '';
                            e.p.afile = afilePrefix + e.p.afile;
                        });

                        const players = entries.map((e) => e.p);

                        if (!Knockout) {
                            KnockoutMain = require('../knockout');
                            Knockout = KnockoutMain.Knockout;
                        }

                        Knockout.find({ g: game, w: { $in: players } }).exec((err, knocks) => {
                            if (err) {
                                return callback.error(err);
                            }

                            knocks.forEach((k) => {
                                let idx = entries.findIndex(
                                    (e) => e.p._id.toString() == k.w.toString()
                                );

                                if (idx != -1) {
                                    ++entries[idx].ks;
                                }
                            });

                            if (!HandModel) {
                                HandModel = require('../hand').Hand;
                            }

                            HandModel.find({ g: game })
                                .populate('p', 'first last afile')
                                .populate('g', 'd m y')
                                .exec((err, highHands) => {
                                    if (err) {
                                        return callback.error(err);
                                    }

                                    // retGame.highHand = highHand

                                    callback.success({ game, entries, highHands });
                                });
                        });

                        //
                    });
            });
    },

    readAllTemp: (callback) => {
        const q = {
            // del: false,
            // s: 3,
        };
        // const gameSelect = 'd m y date s n name';
        // const playerSelect = 'first last nick say afile';

        Game.find(q)
            // .select(gameSelect)
            // .sort({ date: -1 })
            // .limit(1)
            .exec((err, games) => {
                if (err || !games) {
                    return callback.error(err);
                }

                return callback.success(games);
            });
    },

    getIdsForYear: (year) => {
        return new Promise((resolve, reject) => {
            const q = {
                del: false,
                s: 3,
                y: year,
            };

            Game.find(q)
                .select('_id')
                .exec((err, ids) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(ids.map((i) => i._id));
                });
        });
    },

    readAllGameDates: (callback) => {
        const q = {
            del: false,
        };

        Game.find(q)
            .select('date')
            .sort({ date: -1 })
            .exec((err, games) => {
                if (err) {
                    return callback.error(err);
                }

                callback.success(games);
            });
    },

    readGame: (gameId, callback) => {
        Game.findById(gameId)
            // .populate('players', 'first last')
            .exec((err, game) => {
                if (err || !game) {
                    return callback.error(err);
                }

                Entry.find({ g: game })
                    .populate('p', 'first last')
                    .exec((err, entries) => {
                        if (err) {
                            return callback.error(err);
                        }

                        let ret = {
                            _id: game._id,
                            date: game.date,
                            d: game.d,
                            m: game.m,
                            y: game.y,
                            s: game.s,
                            name: game.name,
                            n: game.n,
                            entries: entries,
                        };

                        callback.success(ret);
                    });
            });
    },

    advanceGameState: (gameId, callback) => {
        Game.findById(gameId).exec((err, game) => {
            if (err) {
                return callback.error(err);
            }

            ++game.s;

            game.save((err) => {
                if (err) {
                    return callback.error(err);
                }

                if (game.s == 3) {
                    getRecentGamesIdsToDeactivatePlayers();
                }

                Entry.find({ g: game })
                    .populate('p', 'first last')
                    .exec((err, entries) => {
                        if (err) {
                            return callback.error(err);
                        }

                        let ret = {
                            _id: game._id,
                            date: game.date,
                            d: game.d,
                            m: game.m,
                            y: game.y,
                            s: game.s,
                            n: game.n,
                            entries: entries,
                        };

                        callback.success(ret);
                    });
            });
        });
    },

    readYearReview: (year, callback) => {
        const q = {
            y: year,
            del: false,
            s: 3,
        };

        Game.find(q)
            .select('d m y date n name')
            .sort('date')
            .exec((err, games) => {
                if (err) {
                    return callback.error(err);
                }

                const ids = games.map((d) => d._id);

                EntryMain.getWinnersForGameIds(ids)
                    .then((gameWinners) => {
                        const retGames = games.map((g) => {
                            const { _id, d, m, y, n, name } = g;
                            const winners = gameWinners[_id];

                            return { _id, d, m, y, n, name, winners };
                        });

                        KnockoutMain.findKnockoutsForEntries(retGames)
                            .then((ks) => {
                                retGames.forEach((rg) => {
                                    rg.winners.forEach((w) => {
                                        if (
                                            ks &&
                                            rg._id &&
                                            ks[rg._id] &&
                                            w &&
                                            w.p &&
                                            ks[rg._id][w.p]
                                        ) {
                                            w.ks = ks[rg._id][w.p] || 0;
                                        }
                                    });
                                });

                                // if(!HighHandMain.Hand) {
                                //     HighHandMain = require('../hand')
                                // }

                                const q = {
                                    g: { $in: ids },
                                };

                                if (!HandModel) {
                                    HandModel = require('../hand').Hand;
                                }

                                HandModel.find(q)
                                    .select('g p h')
                                    .populate('g', 'd m y')
                                    .populate('p', 'first last afile')
                                    .exec((err, hhs) => {
                                        if (err) {
                                            return callback.error(err);
                                        }

                                        const hhsObj = {};

                                        hhs.forEach((hh) => {
                                            hhsObj[hh.g._id] = hh;
                                        });

                                        retGames.forEach((g) => {
                                            if (hhsObj[g._id]) {
                                                g.hh = hhsObj[g._id];
                                                // g.hh = {
                                                //     winner: hhsObj[g._id].p,
                                                //     hand: hhsObj[g._id].h,
                                                // };
                                            }
                                        });

                                        Player.populate(
                                            retGames,
                                            { path: 'winners.p', select: 'first last afile' },
                                            (err, data) => {
                                                if (err) {
                                                    return callback.error(err);
                                                }
                                                // Player.populate(
                                                //     data,
                                                //     {
                                                //         path: 'hh.winner',
                                                //         select: 'first last afile',
                                                //     },
                                                //     (err, data) => {
                                                //         if (err) {
                                                //             return callback.error(err);
                                                //         }
                                                callback.success(data);
                                                //     },
                                                // );
                                            }
                                        );
                                    });

                                /*
                                HighHandMain.getHighHandsForSpecificGames(ids)
                                    .then(hhs => {

                                        retGames.forEach(g => {
                                            g.hh = {
                                                winner: hhs[g._id].p,
                                                hand: hhs[g._id].h
                                            }
                                        })
                                        // callback.success(hhs)
                                        callback.success(retGames)
                                    })
                                    .catch(err => {
                                        callback.error(err)
                                    })
                                    */
                            })
                            .catch((err) => {
                                callback.error(err);
                            });
                    })
                    .catch((err) => {
                        callback.error(err);
                    });
            });
    },

    getAllGameIdsAndDates: (callback) => {
        const q = {
            del: false,
            s: 3,
        };

        Game.find(q)
            .select('_id date')
            .sort({ date: -1 })
            .exec((err, docs) => {
                if (err) {
                    return callback.error(err);
                }

                callback.success(docs);

                // const idx = docs.findIndex(g => String(g._id) == gameId)

                // if(idx == -1) {
                //     return callback.error('Incoming game ID not found!')
                // }

                // const retIdx = prev ? idx + 1 : idx - 1

                // console.log(idx, prev, retIdx)

                // if(docs[retIdx]) {
                //     callback.success({_id: docs[retIdx]._id})
                // }else{
                //     callback.success({_id: null})
                // }
            });
    },

    // getNeighborGameId : (gameId, prev, callback) => {

    //     const q = {
    //         del : false,
    //         s: 3
    //     }

    //     Game.find(q)
    //         .select('_id')
    //         .sort({'date': -1})
    //         .exec((err, docs) => {

    //             if(err) {
    //                 return callback.error(err)
    //             }

    //             const idx = docs.findIndex(g => String(g._id) == gameId)

    //             if(idx == -1) {
    //                 return callback.error('Incoming game ID not found!')
    //             }

    //             const retIdx = prev ? idx + 1 : idx - 1

    //             console.log(idx, prev, retIdx)

    //             if(docs[retIdx]) {
    //                 callback.success({_id: docs[retIdx]._id})
    //             }else{
    //                 callback.success({_id: null})
    //             }
    //         })
    // }
};

/*
if (process.env.NODE_ENV != 'copy') {
    console.log('COPY THAT SHIT - game.js');

    requestify
        .request(`http://localhost:4321/api/games`, {
            method: 'GET',
            dataType: 'json',
        })
        .then((response) => {
            const old = JSON.parse(response.body);
            // console.log(old);

            console.log('found', old.length);

            const all = old.map((e) => {
                const copy = { ...e };

                delete copy.__v;

                return Game.findByIdAndUpdate(e._id, copy, { upsert: true });
            });

            Promise.all(all)
                .then((resp) => {
                    console.log('done!', resp.length);
                })
                .catch((err) => {
                    console.log({ err });
                });
        })
        .catch((err) => {
            console.log({ err });
        });
}
*/
