const Games = require('./');
const gamesRouter = require('express').Router();
const auth = require('../../misc/').auth;
const callback = require('../../misc/').routerCallback;

module.exports = (router) => {
    gamesRouter.post('/', auth.isAdmin, (req, res) => {
        Games.create(req.body, callback(res));
    });

    gamesRouter.post('/game/:gameId', auth.isAdmin, (req, res) => {
        Games.saveGame(req.params.gameId, req.body, callback(res));
    });

    /*gamesRouter.post('/:gameId/changeDate', auth.isAdmin, (req, res) => {
        Games.changeDate(req.params.gameId, req.body, callback(res))
    })

    gamesRouter.post('/:gameId/addPlayer', auth.isAdmin, (req, res) => {
        Games.addPlayer(req.params.gameId, req.body._id, callback(res))
    })

    gamesRouter.post('/:gameId/removePlayer', auth.isAdmin, (req, res) => {
        Games.removePlayer(req.params.gameId, req.body._id, callback(res))
    })*/

    // gamesRouter.get('/', (req, res) => {
    //     Games.readAllTemp(callback(res));
    // });

    gamesRouter.get('/latest', auth.isLoggedIn, (req, res) => {
        Games.readLatest(req.hostname == 'localhost', callback(res));
    });

    gamesRouter.get('/dates', auth.isLoggedIn, (req, res) => {
        Games.readAllGameDates(callback(res));
    });

    gamesRouter.get('/datesAndIds', auth.isLoggedIn, (req, res) => {
        Games.getAllGameIdsAndDates(callback(res));
    });

    gamesRouter.get('/all/:year', auth.isLoggedIn, (req, res) => {
        Games.readAll(req.params.year, callback(res));
    });

    gamesRouter.get('/count/:year', auth.isLoggedIn, (req, res) => {
        Games.readCount(req.params.year, callback(res));
    });

    gamesRouter.get('/game/:gameId', auth.isLoggedIn, (req, res) => {
        Games.readGame(req.params.gameId, callback(res));
    });

    gamesRouter.get('/game/:gameId/state', auth.isAdmin, (req, res) => {
        Games.advanceGameState(req.params.gameId, callback(res));
    });

    gamesRouter.get('/review/:year', auth.isLoggedIn, (req, res) => {
        Games.readYearReview(req.params.year, callback(res));
    });

    // gamesRouter.get('/neighbor/:gameId/:prev', auth.isAdmin, (req, res) => {
    //     Games.getNeighborGameId(req.params.gameId, req.params.prev == 'true', callback(res))
    // })

    router.use('/games', gamesRouter);
};
