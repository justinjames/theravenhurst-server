const mongoose = require('mongoose');
// const playerSchema = require('../player').playerSchema
const gameSchema = mongoose.Schema(
    {
        d: { type: Number, required: true, index: true },
        m: { type: Number, required: true, index: true },
        y: { type: Number, required: true, index: true },
        s: { type: Number, default: 0 },
        date: { type: Date, index: true },
        // players: [ { type: mongoose.Schema.ObjectId, ref: "Player" } ],
        // created: { type: Date, default: Date.now },
        del: { type: Boolean, default: false, index: true },
        name: { type: String, default: '' },
        n: { type: String, default: '' },
    },
    {
        timestamps: true,
    }
);
const Game = mongoose.model('Game', gameSchema);

module.exports = {
    Game,
    gameSchema,
};
