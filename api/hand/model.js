const mongoose = require('mongoose')
const handSchema = mongoose.Schema({
    g: { type: mongoose.Schema.ObjectId, ref: "Game", required: true, index: true },
    p: { type: mongoose.Schema.ObjectId, ref: "Player", required: true, index: true },
    h: { type: String, required: true }
}, {
    timestamps: true
})
const Hand = mongoose.model('Hand', handSchema)

module.exports = {
    Hand,
    handSchema
}