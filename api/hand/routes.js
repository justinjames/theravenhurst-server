const Hands = require('./');
const handRouter = require('express').Router();
const auth = require('../../misc/').auth;
const callback = require('../../misc/').routerCallback;

module.exports = (router) => {
    handRouter.post('/', auth.isAdmin, (req, res) => {
        Hands.create(req.body, callback(res));
    });

    // handRouter.post('/:handId', auth.isAdmin, (req, res) => {
    //     Hands.update(req.params.handId, req.body, callback(res))
    // })

    handRouter.delete('/:handId', auth.isAdmin, (req, res) => {
        Hands.delete(req.params.handId, callback(res));
    });

    handRouter.get('/', auth.isLoggedIn, (req, res) => {
        Hands.readAll(callback(res));
    });

    handRouter.get('/game/:gameId', auth.isLoggedIn, (req, res) => {
        Hands.readByGame(req.params.gameId, callback(res));
    });

    handRouter.get('/year/:year', auth.isLoggedIn, (req, res) => {
        Hands.readByYear(req.params.year, callback(res));
    });

    handRouter.get('/:handId', auth.isLoggedIn, (req, res) => {
        Hands.readEntry(req.params.handId, callback(res));
    });

    router.use('/hands', handRouter);
};
