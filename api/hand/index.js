const requestify = require('requestify');

const Hand = require('./model').Hand;
const handSchema = require('./model').handSchema;
let Games;
const Entry = require('../entry');

function findHands(q, callback, single) {
    Hand.find(q)
        .populate('p', 'first last afile')
        .populate('g', 'd m y')
        .exec((err, hands) => {
            if (err) {
                return callback.error(err);
            }

            if (single) {
                return callback.success(hands[0] || {});
            }

            callback.success(hands);
        });
}

module.exports = {
    Hand,
    handSchema,

    create: (newHand, callback) => {
        /*Hand.create(newHand, (err, hand) => {
            if (err) {
                return callback.error(err);
            }

            Hand.findById(hand._id)
                .populate('p', 'first last afile')
                .populate('g', 'd m y')
                .exec((err, hand) => {
                    if (err) {
                        return callback.error(err);
                    }

                    callback.success(hand);
                });
        });*/

        const { g, h, p } = newHand;

        // console.log({ g, h, p });

        Hand.update({ g }, { g, h, p }, { upsert: true }, (err, data) => {
            if (err) {
                return callback.error(err);
            }

            // console.log(hand);

            Hand.findOne({ g })
                .populate('p', 'first last afile')
                .populate('g', 'd m y')
                .exec((err, hand) => {
                    if (err) {
                        return callback.error(err);
                    }

                    callback.success(hand);
                });
        });
    },

    update: (handId, handData, callback) => {
        Hand.findByIdAndUpdate(handId, handData, { new: true }, (err, hand) => {
            if (err) {
                return callback.error(err);
            } else if (!hand) {
                return callback.error(`Hand not found.`);
            }
            callback.success(hand);
        });
    },

    delete: (handId, callback) => {
        Hand.findByIdAndRemove(handId, (err, res) => {
            if (err) {
                return callback.error(err);
            }
            let resObj = res ? { deleted: true } : { notFound: true };
            callback.success(resObj);
        });
    },

    readAll: (callback) => {
        const q = {};
        findHands(q, callback, false);
    },

    readByGame: (gameId, callback) => {
        const q = {
            g: gameId,
        };
        findHands(q, callback, false);
    },

    readByYear: (year, callback) => {
        if (!Games) {
            Games = require('../game');
        }

        Games.getIdsForYear(year)
            .then((ids) => {
                Hand.find({ g: { $in: ids } })
                    .populate('p', 'first last afile')
                    .populate('g', 'd m y')
                    .exec((err, hands) => {
                        if (err) {
                            return callback.error(err);
                        }

                        Entry.getEntriesFromGames(ids)
                            .then((data) => {
                                const ret = [];

                                hands.forEach((h) => {
                                    const obj = {
                                        highHandValue: 0,
                                        g: h.g,
                                        p: h.p,
                                        h: h.h,
                                    };
                                    const entries = data.filter(
                                        (e) => e.g.toString() == h.g._id.toString() && e.hh == 1
                                    );

                                    obj.highHandValue = entries.length * 5;

                                    ret.push(obj);
                                });

                                callback.success(ret);
                            })
                            .catch((err) => {
                                console.log(err);
                                callback.error(err);
                            });
                    });
            })
            .catch((err) => {
                callback.error(err);
            });
    },

    readHand: (handId, callback) => {
        Hand.findById(handId)
            .populate('p', 'first last afile')
            .populate('g', 'd m y')
            .exec((err, hand) => {
                if (err) {
                    return callback.error(err);
                }

                callback.success(hand);
            });
    },

    getAllHighHands: (playerId, gameIds) => {
        const q = {
            p: playerId,
            g: { $in: gameIds },
        };

        return Hand.find(q).populate('g', 'd m y');
    },

    // getHighHandsForSpecificGames: (ids) => {
    //     return new Promise((resolve, reject) => {

    //         const q = {
    //             g: {$in: ids}
    //         }

    //         Hand.find(q)
    //             .select('g p h')
    //             .exec((err, hhs) => {
    //                 if(err) {
    //                     return reject(err)
    //                 }

    //                 const ret = {}

    //                 hhs.forEach(hh => {
    //                     ret[hh.g] = {
    //                         p: hh.p,
    //                         h: hh.h
    //                     }
    //                 })

    //                 resolve(ret)
    //             })
    //     })
    // }
};

/*
if (process.env.NODE_ENV != 'copy') {
    console.log('COPY THAT SHIT - hand.js');

    requestify
        .request(`http://localhost:4321/api/hands`, {
            method: 'GET',
            dataType: 'json',
        })
        .then((response) => {
            const old = JSON.parse(response.body);
            // console.log(old);

            console.log('found', old.length);

            const all = old.map((e) => {
                const copy = { ...e };

                delete copy.__v;

                return Hand.findByIdAndUpdate(e._id, copy, { upsert: true });
            });

            Promise.all(all)
                .then((resp) => {
                    console.log('done!', resp.length);
                })
                .catch((err) => {
                    console.log({ err });
                });
        })
        .catch((err) => {
            console.log({ err });
        });
}
*/
