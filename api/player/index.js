const requestify = require('requestify');

const Player = require('./model').Player;
const playerSchema = require('./model').playerSchema;
const Entry = require('../entry');
const EntryModel = require('../entry').Entry;
const Knockout = require('../knockout');
const encryption = require('../../misc/encryption');
const sendmail = require('sendmail')();
const misc = require('../../misc');
const fs = require('fs');
const Hand = require('../hand');
const Game = require('../game');
const constants = require('../../misc').constants;

function updatePlayer(playerId, playerData, callback, sendEmail, password, updateType) {
    if (playerData.email == '') {
        playerData.email = null;
    }

    Player.findByIdAndUpdate(playerId, playerData, { new: true }, (err, player) => {
        if (err) {
            if (err.toString().indexOf('E11000') > -1) {
                err = `This email address is already in use.`;
            }
            return callback.error(err);
        } else if (!player) {
            return callback.error(`Player not found.`);
        }

        if (sendEmail) {
            let emailSubj = '';
            let emailBody = '';

            if (updateType == 'email') {
                emailSubj = `Your TheRavenhurst.com account email address has been changed or added!`;
                emailBody = `<img style="width: 60%; margin: 10px auto;" src="https://www.theravenhurst.com/assets/full-logo.svg"><br><br>Scott just added or updated the email address associated with an account on <a href="https://www.theravenhurst.com/">TheRavenhurst.com</a>.<br><br>Login with your email address that received this email and use password: ${password}<br><br>You can change your password in your profile once you have logged in.<br><br>Please do not reply to this email.`;
            } else {
                emailSubj = `Your TheRavenhurst.com password has changed!`;
                emailBody = `<img style="width: 60%; margin: 10px auto;" src="https://www.theravenhurst.com/assets/full-logo.svg"><br><br>You or Scott just updated the password associated with your account on <a href="https://www.theravenhurst.com/">TheRavenhurst.com</a>.<br><br>Login with your email address that received this email and use this new password: ${password}<br><br>You can change your password in your profile once you have logged in.<br><br>Please do not reply to this email.`;
            }

            emailPlayer(player.email, emailSubj, emailBody);
        }

        let returnPlayer = {
            first: player.first,
            last: player.last,
            email: player.email,
            nick: player.nick,
            say: player.say,
            phone: player.phone,
        };

        callback.success(returnPlayer);
    });
}

function emailPlayer(address, subject, msg) {
    sendmail({
        from: 'scott@theravenhurst.com',
        to: [address],
        subject: subject,
        html: msg,
    });
}

function generateRandomPassword() {
    const chars = '01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ*$@';
    const len = 8;
    let pass = '';

    while (pass.length < len) {
        const rand = Math.floor(Math.random() * chars.length);
        pass += chars[rand];
    }

    return pass;
}

function getPlayerStats(playerId, year, attachPlayerToResults) {
    return new Promise((resolve, reject) => {
        Game.getAllGameIdsForYear(year)
            .then((res) => {
                const ids = res.map((i) => i._id);
                const all = [
                    Entry.getTotalEntries(playerId, ids),
                    Entry.getTotalPoints(playerId, ids),
                    Entry.getTotalBuyins(playerId, ids),
                    Entry.getTotalPotMoneyWon(playerId, ids),
                    Entry.getTotalHighHandEntries(playerId, ids),
                    Entry.getLongestStreak(playerId, ids),
                    Knockout.getKnockouts(true, playerId, ids),
                    Knockout.getKnockouts(false, playerId, ids),
                    Knockout.getBounties(true, playerId, ids),
                    Knockout.getBounties(false, playerId, ids),
                    Hand.getAllHighHands(playerId, ids),
                    Entry.readBiggestCashByPlayer(playerId, ids),
                    Entry.getTotalThirdPlaceFinishes(playerId, ids),
                    Knockout.getMostInAGame(playerId, ids),
                    Entry.getTotalRebuysForYear(playerId, ids),
                    Entry.getTotalGamesWithoutRebuy(playerId, ids),
                    Entry.getTotalWinsWithoutRebuy(playerId, ids),
                ];

                Promise.all(all)
                    .then((res) => {
                        const totalEntries = res[0]; // total tourneys played, not total buy ins
                        const totalPoints = res[1];
                        const totalBuyIns = res[2];
                        const totalEntryCost = totalBuyIns * 20;
                        const totalPotMoneyWon = res[3];
                        const totalHighHandEntries = res[4];
                        const longestStreak = res[5].streak;
                        const longestStreakLength = res[5].length;
                        const longestStreakActive = res[5].active;
                        const knockoutWins = res[6];
                        const knockoutLosses = res[7];
                        const bountiesWon = res[8];
                        const bountiesLost = res[9];
                        const highHands = res[10];
                        const highestCash = res[11].highestCash;
                        const totalThirdPlaceFinishes = res[12];
                        const highHandsWon = highHands.length;
                        const totalGamesForYear = ids.length;
                        const attendance = totalEntries / totalGamesForYear;
                        const winPercent = totalPoints / totalEntries;
                        const potNet = totalPotMoneyWon - totalEntryCost;
                        const bountiesNet = (bountiesWon - bountiesLost) * constants.bountyCost;
                        const mostKosInAGame = res[13];
                        const mostKosGame = mostKosInAGame.gameId;
                        const mostKos = mostKosInAGame.count;
                        const totalRebuys = res[14];
                        const totalGamesWithoutRebuy = res[15];
                        const totalWinsWithoutRebuy = res[16];

                        let highHandsNet = 0;
                        let totalEliminations = 0;
                        let totalEliminated = 0;

                        const buyinRatio = totalBuyIns / totalEntries;

                        if (knockoutWins && knockoutWins.length > 0) {
                            const knockoutTotals = knockoutWins.map((w) => w.total);

                            totalEliminations = knockoutTotals.reduce((p, c) => {
                                return p + c;
                            });
                        }

                        if (knockoutLosses && knockoutLosses.length > 0) {
                            const knockoutTotals = knockoutLosses.map((w) => w.total);

                            totalEliminated = knockoutTotals.reduce((p, c) => {
                                return p + c;
                            });
                        }

                        const knockoutRatio = (totalEliminations - totalEliminated) / totalEntries;

                        const all2 = [
                            Entry.getHighHandWinnings(playerId, highHands),
                            Player.findById(playerId).select('first last afile'),
                        ];

                        Promise.all(all2)
                            .then(([highHandWins, player]) => {
                                if (totalHighHandEntries && totalHighHandEntries > 0) {
                                    highHandsNet =
                                        (highHandWins - totalHighHandEntries) *
                                        constants.highHandCost;
                                }

                                const overallNet = potNet + bountiesNet + highHandsNet;
                                const netRatio = overallNet / totalEntries;

                                const results = {
                                    totalGamesForYear,
                                    totalEntries,
                                    buyinRatio,
                                    attendance,
                                    totalPoints,
                                    winPercent,
                                    totalEntryCost,
                                    totalPotMoneyWon,
                                    highestCash,
                                    totalThirdPlaceFinishes,
                                    potNet,
                                    bountiesWon,
                                    bountiesLost,
                                    bountiesNet,
                                    totalHighHandEntries,
                                    longestStreak,
                                    longestStreakLength,
                                    longestStreakActive,
                                    highHandsNet,
                                    highHandsWon,
                                    overallNet,
                                    netRatio,
                                    totalEliminations,
                                    totalEliminated,
                                    knockoutWins,
                                    knockoutLosses,
                                    knockoutRatio,
                                    highHands,
                                    // mostKosInAGame,
                                    mostKosGame,
                                    mostKos,
                                    totalRebuys,
                                    totalGamesWithoutRebuy,
                                    totalWinsWithoutRebuy,
                                };

                                if (attachPlayerToResults) {
                                    results.player = player;
                                }

                                resolve(results);
                            })
                            .catch((err) => {
                                // callback.error(err)
                                reject(err);
                            });
                    })
                    .catch((err) => {
                        // console.log('ERR', err)
                        // callback.error(err)
                        reject(err);
                    });
            })
            .catch((err) => {
                // callback.error(err)
                reject(err);
            });
    });
}

function getStatsAverage(year) {
    return new Promise((resolve, reject) => {
        Game.getAllGameIdsForYear(year)
            .then((res) => {
                const ids = res.map((i) => i._id);

                Entry.getUniquePlayers(ids)
                    .then((res) => {
                        const all = [];
                        const numbPlayers = res.length;

                        res.forEach((pid) => {
                            all.push(getPlayerStats(pid, year, false));
                        });

                        Promise.all(all)
                            .then((res) => {
                                const ret = {};
                                const ret2 = {};

                                res.forEach((playersStats) => {
                                    for (let prop in playersStats) {
                                        if (typeof playersStats[prop] == 'number') {
                                            if (!ret[prop]) {
                                                ret[prop] = {
                                                    count: 0,
                                                };
                                            }

                                            ret[prop].count += playersStats[prop];
                                        }
                                    }
                                });

                                for (let prop in ret) {
                                    ret2[prop] = ret[prop].count / numbPlayers;
                                }

                                resolve(ret2);
                            })
                            .catch((err) => {
                                reject(err);
                            });
                    })
                    .catch((err) => {
                        reject(err);
                    });
            })
            .catch((err) => {
                reject(err);
            });
    });
}

module.exports = {
    Player,
    playerSchema,

    setToActive: (playerId) => {
        Player.findByIdAndUpdate(playerId, { active: true }, (err, doc) => {
            if (err) {
                return console.log(err);
            }
        });
    },

    create: (newPlayer, local, callback) => {
        let sendEmail = false;

        if (newPlayer.email) {
            newPlayer.email = newPlayer.email.toLowerCase();
            sendEmail = true;
        } else if (newPlayer.email == '') {
            delete newPlayer.email;
        }

        if (newPlayer.password == '') {
            delete newPlayer.password;
        }

        if (!newPlayer.password && sendEmail) {
            newPlayer.password = generateRandomPassword();
        }

        newPlayer.salt = encryption.createSalt();

        if (newPlayer.password) {
            newPlayer.hashed = encryption.hashPassword(newPlayer.salt, newPlayer.password);
        }

        newPlayer.afile = misc.getRandomDonkey();
        newPlayer.first = newPlayer.first.trim();
        newPlayer.last = newPlayer.last.trim();

        Player.create(newPlayer, (err, player) => {
            if (err) {
                if (err.toString().indexOf('E11000') > -1) {
                    err = `This email address is already in use.`;
                }
                return callback.error(err);
            }

            const emailSubj = `Your TheRavenhurst.com account was created!`;
            const emailBody = `<img style="width: 60%; margin: 10px auto;" src="https://www.theravenhurst.com/assets/full-logo.svg"><br><br>Congrats, Scott was kind enough to create an account for you on <a href="https://www.theravenhurst.com/">TheRavenhurst.com</a>.<br><br>Login with your email address that received this email and use password: ${newPlayer.password}<br><br>You can change your password in your profile once you have logged in.<br><br>Please do not reply to this email.`;

            if (sendEmail) {
                emailPlayer(newPlayer.email, emailSubj, emailBody);
            }

            const afilePrefix = local ? 'http://localhost:1234' : '';
            player.afile = afilePrefix + player.afile;

            callback.success(player);
        });
    },

    update: (playerId, playerData, callback) => {
        Player.findById(playerId)
            .select('salt email')
            .exec((err, player) => {
                if (err) {
                    return callback.error(err);
                }

                let sendEmail = false;
                let password = '';

                if (playerData.email) {
                    playerData.email = playerData.email.toLowerCase();
                    password = generateRandomPassword();
                    playerData.hashed = encryption.hashPassword(player.salt, password);
                    sendEmail = true;
                }

                if (playerData.first) {
                    playerData.first = playerData.first.trim();
                }

                if (playerData.last) {
                    playerData.last = playerData.last.trim();
                }

                updatePlayer(playerId, playerData, callback, sendEmail, password, 'email');
            });
    },

    updatePassword: (playerId, newPass, callback) => {
        Player.findById(playerId)
            .select('email salt')
            .exec((err, player) => {
                if (err) {
                    return callback.error(err);
                }

                const hashed = encryption.hashPassword(player.salt, newPass);
                updatePlayer(playerId, { hashed }, callback, true, newPass, 'password');
            });
    },

    readAll: (local, callback) => {
        const q = {
            // active : true
        };
        const sel =
            'first last nick say perms hide phone createdAt updatedAt login admin afile active';

        Player.find(q)
            // .select('first last display email created login admin afile aurl')
            .select(sel)
            .exec((err, players) => {
                if (err) {
                    return callback.error(err);
                }

                const afilePrefix = local ? 'http://localhost:1234' : '';

                players.forEach((p) => {
                    p.afile = afilePrefix + p.afile;
                });

                callback.success(players);
            });
    },

    readAllPoints: (local, year, callback) => {
        const q = {
            // active : true
        };
        const playerSelect = 'first last afile';

        Player.find(q)
            .select(playerSelect)
            .exec((err, players) => {
                if (err) {
                    return callback.error(err);
                }

                const afilePrefix = local ? 'http://localhost:1234' : '';
                const startDate = new Date(year, 0, 1, 0, 0, 0, 0);
                const endDate = new Date(++year, 0, 1, 0, 0, 0, 0);

                players.forEach((p) => {
                    p.afile = afilePrefix + p.afile;
                });

                EntryModel.aggregate([
                    {
                        $match: {
                            pt: true,
                            date: { $gte: startDate, $lt: endDate },
                        },
                    },
                    {
                        $group: {
                            _id: {
                                player: '$p',
                            },
                            total: { $sum: 1 },
                        },
                    },
                ]).exec((err, entries) => {
                    if (err) {
                        return callback.error(err);
                    }

                    let returnPlayers = [];
                    let props = playerSelect.split(' ');

                    players.forEach((p) => {
                        let tempPlayer = {};
                        let idx = entries.findIndex((e) => {
                            return e._id.player.toString() == p._id.toString();
                        });

                        props.forEach((prop) => {
                            tempPlayer[prop] = p[prop];
                        });

                        if (idx != -1) {
                            tempPlayer.points = entries[idx].total;
                        } else {
                            tempPlayer.points = 0;
                        }

                        if (tempPlayer.points > 0) {
                            returnPlayers.push(tempPlayer);
                        }
                    });

                    callback.success(returnPlayers);
                });
            });
    },

    readPlayer: (playerId, local, callback) => {
        const select =
            'first last email phone nick say perms hide createdAt updatedAt login admin afile aurl hashed';
        const keys = select.split(' ');

        Player.findById(playerId)
            .select(select)
            .exec((err, play) => {
                if (err) {
                    return callback.error(err);
                } else if (!play) {
                    return callback.error('Player not found!');
                }

                const hasPass = play.hashed != undefined;
                let player = {};

                keys.forEach((prop) => {
                    if (prop != 'hashed' && play[prop]) {
                        player[prop] = play[prop];
                    }
                });

                const afilePrefix = local ? 'http://localhost:1234' : '';
                player.afile = afilePrefix + player.afile;

                callback.success({ player, hasPass });
            });
    },

    readStats: (playerId, year, callback) => {
        getPlayerStats(playerId, year, false)
            .then((stats) => {
                callback.success(stats);
            })
            .catch((err) => {
                console.log(err);
                callback.error(err);
            });
    },

    readStatsAverage: (year, callback) => {
        getStatsAverage(year)
            .then((stats) => {
                callback.success(stats);
            })
            .catch((err) => {
                callback.error(err);
            });
    },

    getLeaderboardStats: (year, callback) => {
        Player.find({})
            .select('_id')
            .exec((err, players) => {
                if (err) {
                    return callback.error(err);
                }

                const all = [];

                players.forEach((p) => all.push(getPlayerStats(p._id, year, true)));

                Promise.all(all)
                    .then((stats) => {
                        const ret = stats.filter((s) => s.totalEntries > 0);
                        callback.success(ret);
                    })
                    .catch((err) => {
                        callback.error(err);
                    });
            });
    },

    randomImage: (playerId, local, callback) => {
        Player.findById(playerId).exec((err, play) => {
            if (err) {
                return callback.error(err);
            } else if (!play) {
                return callback.error('Player not found!');
            }

            play.afile = misc.getRandomDonkey();

            play.save((err) => {
                if (err) {
                    return callback.error(err);
                }

                let player = {
                    afile: play.afile,
                };

                const afilePrefix = local ? 'http://localhost:1234' : '';
                player.afile = afilePrefix + player.afile;

                callback.success(player);
            });
        });
    },

    uploadImage: (req, local, storagePath, callback) => {
        const playerId = req.params.playerId;

        Player.findById(playerId).exec((err, play) => {
            const tempPath = req.file.path;
            const filenameArray = req.file.originalname.split('.');
            const targetPath =
                storagePath +
                playerId +
                '.' +
                filenameArray[filenameArray.length - 1].toLowerCase();
            const src = fs.createReadStream(tempPath);
            const dest = fs.createWriteStream(targetPath);
            const oldFileIsStorage = play.afile.search('/storage') == 0;
            const oldFile = '..' + play.afile;

            src.pipe(dest);

            src.on('end', () => {
                fs.unlinkSync(tempPath);

                play.afile = targetPath.slice(2, targetPath.length);

                play.save((err) => {
                    if (err) {
                        return callback.error(err);
                    }

                    if (oldFileIsStorage && oldFile != targetPath) {
                        fs.unlinkSync(oldFile);
                    }

                    let player = {
                        afile: play.afile,
                    };

                    const afilePrefix = local ? 'http://localhost:1234' : '';
                    player.afile = afilePrefix + player.afile;

                    callback.success(player);
                });
            });

            src.on('error', (err) => {
                res.json({ success: false });
            });
        });
    },
};

/*
if (process.env.NODE_ENV != 'copy') {
    console.log('COPY THAT SHIT - player.js');

    requestify
        .request(`http://localhost:4321/api/players`, {
            method: 'GET',
            dataType: 'json',
        })
        .then((response) => {
            const old = JSON.parse(response.body);
            // console.log(old);

            console.log('found', old.length);

            const all = old.map((e) => {
                const copy = { ...e };

                delete copy.__v;

                return Player.findByIdAndUpdate(e._id, copy, { upsert: true });
            });

            Promise.all(all)
                .then((resp) => {
                    console.log('done!', resp.length);
                })
                .catch((err) => {
                    console.log({ err });
                });
        })
        .catch((err) => {
            console.log({ err });
        });
}
*/
