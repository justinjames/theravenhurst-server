const mongoose = require('mongoose')
const encryption = require('../../misc/encryption')
const playerSchema = mongoose.Schema({
    first: { type: String, required: true },
    last: { type: String, required: true },
    nick: { type: String },
    say: { type: String },
    phone: { type: String },
    email: { type: String, unique: true, sparse: true },
    afile: { type: String }, // avatar file on system (either random donkey or uploaded image)
    salt: { type: String, required: true },
    hashed: String,
    login: { type: Date },
    admin: { type: Boolean, default: false },
    active: { type: Boolean, default: true, index: true },
    perms: { type: String },
    hide: { type: String }
}, {
    timestamps: true
})

playerSchema.methods = {
    auth: function(passwordToMatch) {
        return encryption.hashPassword(this.salt, passwordToMatch) === this.hashed
    }
}

const Player = mongoose.model('Player', playerSchema)

module.exports = {
    Player,
    playerSchema
}