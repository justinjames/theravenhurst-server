const Player = require('./');
const playerRouter = require('express').Router();
const auth = require('../../misc/').auth;
const callback = require('../../misc/').routerCallback;
const multer = require('multer');
const storagePath = '../data/storage/';

let upload = multer({ dest: storagePath });
const type = upload.single('recfile');

module.exports = (router) => {
    router.post('/login', auth.authenticate);
    router.post('/logout', auth.logout);

    playerRouter.post('/', auth.isAdmin, (req, res) => {
        Player.create(req.body, req.hostname == 'localhost', callback(res));
    });

    playerRouter.post('/:playerId', auth.isSamePlayerOrAdmin, (req, res) => {
        Player.update(req.params.playerId, req.body, callback(res));
    });

    playerRouter.post('/:playerId/password', auth.isSamePlayerOrAdmin, (req, res) => {
        Player.updatePassword(req.params.playerId, req.body.password, callback(res));
    });

    playerRouter.get('/', auth.isLoggedIn, (req, res) => {
        Player.readAll(req.hostname == 'localhost', callback(res));
    });

    playerRouter.get('/points/:year', auth.isLoggedIn, (req, res) => {
        Player.readAllPoints(req.hostname == 'localhost', req.params.year, callback(res));
    });

    playerRouter.get('/stats/:year', auth.isLoggedIn, (req, res) => {
        Player.getLeaderboardStats(req.params.year, callback(res));
    });

    playerRouter.get('/statsaverage/:year', auth.isLoggedIn, (req, res) => {
        Player.readStatsAverage(req.params.year, callback(res));
    });

    playerRouter.get('/:playerId', auth.isLoggedIn, (req, res) => {
        Player.readPlayer(req.params.playerId, req.hostname == 'localhost', callback(res));
    });

    playerRouter.get('/:playerId/stats/:year', auth.isLoggedIn, (req, res) => {
        Player.readStats(req.params.playerId, req.params.year, callback(res));
    });

    playerRouter.get('/:playerId/randomImage', auth.isSamePlayer, (req, res) => {
        Player.randomImage(req.params.playerId, req.hostname == 'localhost', callback(res));
    });

    playerRouter.post('/:playerId/uploadImage', auth.isSamePlayer, type, (req, res) => {
        Player.uploadImage(req, req.hostname == 'localhost', storagePath, callback(res));
    });

    router.use('/players', playerRouter);

    router.get('/user', (req, res) => {
        res.json({
            user: req.user || null,
        });
    });
};
