const fs = require('fs')
const path = require('path')
const jsonfile = require('jsonfile')

const annPath = path.normalize(__dirname + '/../../../data/announcements')

function readJsonFile(filename, contents) {
    return new Promise((resolve, reject) => {

        const fullFilePath = path.normalize(annPath + '/' + filename)

        jsonfile.readFile(fullFilePath, (err, obj) => {
            
            if(err) {
                reject()
            }else{
                contents[filename] = obj
                resolve()
            }
        })
    })
}

function returnReadJsonFile(filename, contents) {
    return () => {
        return readJsonFile(filename, contents)
    }
}

function chainer(filenames, contents, func) {

    let chain = Promise.resolve()

    filenames.forEach(filename => {
        chain = chain.then(func(filename, contents))
    })

    return chain
}

function getNewFileName() {

    const dat = new Date()
    const d = dat.getDate()
    const m = dat.getMonth()
    const y = dat.getFullYear()
    const t = dat.getTime()

    return y + '-' + m + '-' + d + '-' + t + '.json'
}

function converntFileNameToDate(fn) {

    fn = fn.replace('.json', '')
    let a = fn.split('-')
    let d = new Date(a[0], a[1], a[2], 0,0,0,0)

    d.setTime(a[3])

    return d
}

module.exports = {
    
    readAll: (callback) => {

        fs.readdir(annPath, function(err, filenames) {

            if(err) {
                return
            }

            let contents = {}

            chainer(filenames, contents, returnReadJsonFile).then(() => {
                
                let ret = []

                for(let prop in contents) {
                    ret.push({
                        date: converntFileNameToDate(prop),
                        text: contents[prop].message
                    })
                }

                ret.sort((a, b) => {
                    return b.date.getTime() - a.date.getTime()
                })

                callback.success(ret)
            })
            .catch(err => {
                callback.error(err)
                console.log(`OH CRAP ${err}`)
            })
        })
    },


    create: (body, callback) => {

        const fn = getNewFileName()
        const fullFilePath = path.normalize(annPath + '/' + fn)

        jsonfile.writeFile(fullFilePath, body, err => {
            
            if(err) {
                return callback.error(err)
            }

            // callback.success({success: true, fn})
            callback.success({date: converntFileNameToDate(fn), text: body.message})
        })
    }

}