const annRouter = require('express').Router()
const auth = require('../../misc/').auth
const callback = require('../../misc/').routerCallback
const announcements = require('./')

module.exports = router => {
    
    annRouter.get('/', auth.isLoggedIn, (req, res) => {
        announcements.readAll(callback(res))
    })

    annRouter.post('/', auth.isAdmin, (req, res) => {
        announcements.create(req.body, callback(res))
    })

    router.use('/announcements', annRouter)
}