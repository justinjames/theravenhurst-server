const mongoose = require('mongoose')
const knockoutSchema = mongoose.Schema({
    g: { type: mongoose.Schema.ObjectId, ref: "Game", required: true, index: true },
    w: { type: mongoose.Schema.ObjectId, ref: "Player", index: true },
    l: { type: mongoose.Schema.ObjectId, ref: "Player", required: true, index: true },
    b: { type: Number, default: 0, index: true },
    s: { type : Date } // sort date - added once i started allowing knockouts to be moved up or down in order
}, {
    timestamps: true
})
const Knockout = mongoose.model('Knockout', knockoutSchema)

module.exports = {
    Knockout,
    knockoutSchema
}