const Knockouts = require('./');
const knockoutRouter = require('express').Router();
const auth = require('../../misc/').auth;
const callback = require('../../misc/').routerCallback;

module.exports = (router) => {
    knockoutRouter.post('/', auth.isAdmin, (req, res) => {
        Knockouts.create(req.body, callback(res));
    });

    knockoutRouter.post('/:knockoutId', auth.isAdmin, (req, res) => {
        Knockouts.update(req.params.knockoutId, req.body, callback(res));
    });

    knockoutRouter.delete('/:knockoutId', auth.isAdmin, (req, res) => {
        Knockouts.delete(req.params.knockoutId, callback(res));
    });

    knockoutRouter.get('/', auth.isLoggedIn, (req, res) => {
        Knockouts.readAll(callback(res));
    });

    knockoutRouter.get('/game/:gameId', auth.isLoggedIn, (req, res) => {
        Knockouts.readAllByGame(req.params.gameId, callback(res));
    });

    knockoutRouter.get('/winner/:playerId', auth.isLoggedIn, (req, res) => {
        Knockouts.readAllByWinner(req.params.playerId, callback(res));
    });

    knockoutRouter.get('/loser/:playerId', auth.isLoggedIn, (req, res) => {
        Knockouts.readAllByLoser(req.params.playerId, callback(res));
    });

    knockoutRouter.get('/eliminated/:playerId/:opponentId/:year', auth.isLoggedIn, (req, res) => {
        Knockouts.readTimesEliminatedOther(
            req.params.playerId,
            req.params.opponentId,
            req.params.year,
            callback(res)
        );
    });

    knockoutRouter.get('/:knockoutId', auth.isLoggedIn, (req, res) => {
        Knockouts.readEntry(req.params.knockoutId, callback(res));
    });

    router.use('/knockouts', knockoutRouter);
};
