const requestify = require('requestify');

const Knockout = require('./model').Knockout;
const knockoutSchema = require('./model').knockoutSchema;
const mongoose = require('mongoose');
const Player = require('mongoose').model('Player');
const GamesController = require('../game');

function findKnockouts(q, callback) {
    Knockout.find(q)
        .populate('w', 'first last afile')
        .populate('l', 'first last afile')
        .populate('g', 'd m y')
        .exec((err, entries) => {
            if (err) {
                return callback.error(err);
            }

            callback.success(entries);
        });
}

module.exports = {
    Knockout,
    knockoutSchema,

    create: (newKnockout, callback) => {
        Knockout.create(newKnockout, (err, knockout) => {
            if (err) {
                return callback.error(err);
            }

            Knockout.findById(knockout._id)
                .populate('w', 'first last afile')
                .populate('l', 'first last afile')
                .populate('g', 'd m y')
                .exec((err, entries) => {
                    if (err) {
                        return callback.error(err);
                    }

                    callback.success(entries);
                });
        });
    },

    update: (knockoutId, knockoutData, callback) => {
        // delete knockoutData.game
        // delete knockoutData.player

        Knockout.findByIdAndUpdate(knockoutId, knockoutData, { new: true }, (err, knockout) => {
            if (err) {
                return callback.error(err);
            } else if (!knockout) {
                return callback.error(`Knockout not found.`);
            }
            callback.success(knockout);
        });
    },

    delete: (knockoutId, callback) => {
        Knockout.findByIdAndRemove(knockoutId, (err, res) => {
            if (err) {
                return callback.error(err);
            }
            let resObj = res ? { deleted: true } : { notFound: true };
            callback.success(resObj);
        });
    },

    readAll: (callback) => {
        const q = {};
        findKnockouts(q, callback);
    },

    readAllByGame: (gameId, callback) => {
        const q = {
            g: gameId,
        };
        findKnockouts(q, callback);
    },

    readAllByWinner: (playerId, callback) => {
        const q = {
            w: playerId,
        };
        findKnockouts(q, callback);
    },

    readAllByLoser: (playerId, callback) => {
        const q = {
            l: playerId,
        };
        findKnockouts(q, callback);
    },

    readTimesEliminatedOther: (playerId, opponentId, year, callback) => {
        const q = {
            w: playerId,
            l: opponentId,
        };

        GamesController.getAllGameIdsForYear(year)
            .then((ids) => {
                q.g = { $in: ids };

                Knockout.find(q)
                    .select('g')
                    .populate('g', 'd m y')
                    .exec((err, docs) => {
                        if (err) {
                            return callback.error(err);
                        }

                        callback.success(docs);
                    });
            })
            .catch((er) => {
                callback.error(err);
            });
    },

    readKnockout: (knockoutId, callback) => {
        Knockout.findById(knockoutId)
            .populate('w', 'first last afile')
            .populate('l', 'first last afile')
            .populate('g', 'd m y')
            .exec((err, knockout) => {
                if (err) {
                    return callback.error(err);
                }

                callback.success(knockout);
            });
    },

    getKnockouts: (wins, playerId, gameIds) => {
        return new Promise((resolve, reject) => {
            const q = {
                g: { $in: gameIds },
            };
            const groupId = {};

            if (wins) {
                q.w = mongoose.Types.ObjectId(playerId);
                groupId.player = '$l';
            } else {
                q.l = mongoose.Types.ObjectId(playerId);
                groupId.player = '$w';
            }

            Knockout.aggregate(
                [
                    {
                        $match: q,
                    },
                    {
                        $group: {
                            _id: groupId,
                            total: { $sum: 1 },
                            bounties: { $sum: '$b' },
                        },
                    },
                ],
                (err, knockouts) => {
                    if (err) {
                        return reject(err);
                    }

                    Player.populate(
                        knockouts,
                        { path: '_id.player', select: 'first last' },
                        (err, data) => {
                            if (err) {
                                return reject(err);
                            }

                            // let ret = data.filter(d => d._id.player)

                            let ret = data.map((d) => {
                                const winner = d._id.player
                                    ? d._id.player
                                    : {
                                          _id: null,
                                          first: 'missing',
                                          last: 'missing',
                                      };

                                return {
                                    _id: winner._id,
                                    first: winner.first,
                                    last: winner.last,
                                    total: d.total,
                                    bounties: d.bounties,
                                };
                            });

                            ret = ret.sort((a, b) => {
                                if (a.total == b.total) {
                                    let an = a.first.toLowerCase();
                                    let bn = b.first.toLowerCase();

                                    if (an == 'missing') {
                                        an = 'zzzzzzzzzzz';
                                    }

                                    if (bn == 'missing') {
                                        bn = 'zzzzzzzzzzz';
                                    }

                                    if (an == bn) {
                                        an = a.last.toLowerCase();
                                        bn = b.last.toLowerCase();
                                    }

                                    if (an < bn) {
                                        return -1;
                                    } else if (an > bn) {
                                        return 1;
                                    }

                                    return 0;
                                } else {
                                    return b.total - a.total;
                                }
                            });

                            resolve(ret);
                        }
                    );
                }
            );
        });
    },

    /*getKnockoutsAverage : (gameIds) => {

        return new Promise((resolve, reject) => {

            const q = {
                g: {$in: gameIds}
            }
            const groupId = null

            Knockout.aggregate([
                {
                    $match: q
                },{
                    $group: {
                        _id: groupId,
                        total: {$sum: 1},
                        bounties: {$sum: '$b'}
                    }
                }
            ], (err, knockouts) => {

                if(err) {
                    return reject(err)
                }

                // Player.populate(knockouts, {path: '_id.player', select: 'first last'}, (err, data) => {

                    // if(err) {
                    //     return reject(err)
                    // }

                    // let ret = data.map(d => {

                    //     const winner = d._id.player ? d._id.player : { _id: null, first : 'missing', last : 'missing' }

                    //     return {
                    //         _id: winner._id,
                    //         first: winner.first,
                    //         last: winner.last,
                    //         total: d.total,
                    //         bounties: d.bounties
                    //     }
                    // })

                    // ret = ret.sort((a, b) => {
                    //     if(a.total == b.total) {

                    //         let an = a.first.toLowerCase()
                    //         let bn = b.first.toLowerCase()

                    //         if(an == 'missing') {
                    //             an = 'zzzzzzzzzzz'
                    //         }

                    //         if(bn == 'missing') {
                    //             bn = 'zzzzzzzzzzz'
                    //         }

                    //         if(an == bn) {
                    //             an = a.last.toLowerCase()
                    //             bn = b.last.toLowerCase()
                    //         }

                    //         if(an < bn) {
                    //             return -1
                    //         }else if(an > bn) {
                    //             return 1
                    //         }

                    //         return 0
                    //     }else{
                    //         return b.total - a.total
                    //     }
                    // })
    
                    resolve(knockouts)
                // })
            })
        })
    },*/

    getBounties: (wins, playerId, gameIds) => {
        return new Promise((resolve, reject) => {
            const q = {
                g: { $in: gameIds },
            };

            if (wins) {
                q.w = mongoose.Types.ObjectId(playerId);
            } else {
                q.l = mongoose.Types.ObjectId(playerId);
            }

            Knockout.aggregate(
                [
                    {
                        $match: q,
                    },
                    {
                        $group: {
                            _id: null,
                            bounties: { $sum: '$b' },
                        },
                    },
                ],
                (err, knockouts) => {
                    if (err) {
                        return reject(err);
                    }

                    resolve(knockouts[0] ? knockouts[0].bounties : 0);
                }
            );
        });
    },

    getMostInAGame: (playerId, gameIds) => {
        return new Promise((resolve, reject) => {
            const q = {
                g: { $in: gameIds },
                w: mongoose.Types.ObjectId(playerId),
            };

            Knockout.aggregate(
                [
                    {
                        $match: q,
                    },
                    {
                        $group: {
                            _id: '$g',
                            count: { $sum: 1 },
                        },
                    },
                ],
                (err, knockouts) => {
                    if (err) {
                        return reject(err);
                    } else if (!knockouts || !knockouts.length) {
                        return resolve({ gameId: '', count: 0 });
                    }

                    knockouts = knockouts.sort((a, b) => {
                        return b.count - a.count;
                    });

                    const bestGame = knockouts[0];
                    const { _id, count } = bestGame;
                    const gameId = _id;

                    resolve({ gameId, count });
                }
            );
        });
    },

    findKnockoutsForEntries: (games) => {
        return new Promise((resolve, reject) => {
            const q = {
                $or: [],
            };

            games.forEach((g) => {
                g.winners.forEach((gw) => {
                    q['$or'].push({
                        g: g._id,
                        w: gw.p,
                    });
                });
            });

            Knockout.aggregate([
                {
                    $match: q,
                },
                {
                    $group: {
                        _id: '$g',
                        ps: { $push: '$w' },
                    },
                },
            ]).exec((err, kos) => {
                if (err) {
                    return reject(err);
                }

                const ret = {};

                kos.forEach((g) => {
                    ret[g._id] = {};

                    g.ps.forEach((p) => {
                        if (!ret[g._id][p]) {
                            ret[g._id][p] = 0;
                        }

                        ++ret[g._id][p];
                    });
                });

                resolve(ret);
            });
        });
    },

    /*getBountiesAverage : (gameIds) => {

        return new Promise((resolve, reject) => {

            const q = {
                g: {$in: gameIds}
            }

            // if(wins) {
            //     q.w = mongoose.Types.ObjectId(playerId)
            // }else{
            //     q.l = mongoose.Types.ObjectId(playerId)
            // }

            Knockout.aggregate([
                {
                    $match: q
                },{
                    $group: {
                        _id: null,
                        bounties: {$sum: '$b'}
                    }
                }
            ], (err, knockouts) => {

                if(err) {
                    return reject(err)
                }

                // console.log('', )

                resolve(knockouts)

                // resolve(knockouts[0] ? knockouts[0].bounties : 0)
            })
        })
    }*/
};

/*
if (process.env.NODE_ENV != 'copy') {
    console.log('COPY THAT SHIT - knockout.js');

    requestify
        .request(`http://localhost:4321/api/knockouts`, {
            method: 'GET',
            dataType: 'json',
        })
        .then((response) => {
            const old = JSON.parse(response.body);
            // console.log(old);

            console.log('found', old.length);

            const all = old.map((e) => {
                const copy = { ...e };

                delete copy.__v;

                return Knockout.findByIdAndUpdate(e._id, copy, { upsert: true });
            });

            Promise.all(all)
                .then((resp) => {
                    console.log('done!', resp.length);
                })
                .catch((err) => {
                    console.log({ err });
                });
        })
        .catch((err) => {
            console.log({ err });
        });
}
*/
