const router = require('express').Router()

// const e = require('./entry')
// const p = require('./player')

require('./player/routes')(router)
require('./entry/routes')(router)
require('./game/routes')(router)
require('./hand/routes')(router)
require('./knockout/routes')(router)
require('./announcements/routes')(router)
require('./messages/routes')(router)

module.exports = app => app.use('/api', router)