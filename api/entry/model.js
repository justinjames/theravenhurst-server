const mongoose = require('mongoose')
const entrySchema = mongoose.Schema({
    g: { type: mongoose.Schema.ObjectId, ref: "Game", required: true, index: true }, // game
    date: { type: Date, index: true }, // date of game
    p: { type: mongoose.Schema.ObjectId, ref: "Player", required: true, index: true }, // player
    pt: { type: Boolean, default: false, index: true }, // got points
    c: { type: Number, default: 0 }, // cash amount
    b: { type: Number, default: 1 }, // number of buy ins
    q: { type: Number, default: 1, index: true }, // qualifies
    hh: { type : Number, default: 0 }
}, {
    timestamps: true
})
const Entry = mongoose.model('Entry', entrySchema)

module.exports = {
    Entry,
    entrySchema
}