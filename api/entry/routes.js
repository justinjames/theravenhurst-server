const Entry = require('./');
const entryRouter = require('express').Router();
const auth = require('../../misc/').auth;
const callback = require('../../misc/').routerCallback;

module.exports = (router) => {
    entryRouter.post('/', auth.isAdmin, (req, res) => {
        Entry.create(req.body, callback(res));
    });

    entryRouter.post('/:entryId', auth.isAdmin, (req, res) => {
        Entry.update(req.params.entryId, req.body, callback(res));
    });

    entryRouter.delete('/:entryId', auth.isAdmin, (req, res) => {
        Entry.delete(req.params.entryId, callback(res));
    });

    entryRouter.get('/', auth.isLoggedIn, (req, res) => {
        Entry.readAll(callback(res));
    });

    entryRouter.get('/game/:gameId', auth.isLoggedIn, (req, res) => {
        Entry.readAllByGame(req.params.gameId, callback(res));
    });

    entryRouter.get('/player/:playerId', auth.isLoggedIn, (req, res) => {
        Entry.readAllByPlayer(req.params.playerId, callback(res));
    });

    // entryRouter.get('/highestPot/:playerId', (req, res) => {
    //     Entry.readBiggestCashByPlayer(req.params.playerId, callback(res))
    // })

    entryRouter.get('/:entryId', auth.isLoggedIn, (req, res) => {
        Entry.readEntry(req.params.entryId, callback(res));
    });

    router.use('/entries', entryRouter);
};
