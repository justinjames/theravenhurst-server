const requestify = require('requestify');

const Entry = require('./model').Entry;
const entrySchema = require('./model').entrySchema;
const mongoose = require('mongoose');
let Player = require('../player/index');
// const EntryMod = require('mongoose').model('Entry')
// const Game = require('../game').Game

function findEntries(q, callback) {
    Entry.find(q)
        .populate('p', 'first last')
        .populate('g', 'd m y')
        .exec((err, entries) => {
            if (err) {
                return callback.error(err);
            }

            callback.success(entries);
        });
}

function getPlayerLongestStreak(entries) {
    return new Promise((resolve, reject) => {
        // console.log(entries)
        // console.log(gameIds)

        let longest = [];
        let cur = [];
        let idxCount;
        let active = false;

        // gameIds.forEach(id => {
        //     console.log('looking for', id)
        //     const idx = entries.findIndex(e => String(e.g) == String(id))
        //     console.log(idx)
        // })

        entries.forEach((e) => {
            // console.log('looking for', e.g)

            if (e.pt) {
                // const idx = gameIds.findIndex(gid => String(e.g) == String(gid))
                // console.log(idx)
                // console.log(cur)

                cur.push({
                    date: e.date,
                    game: e.g,
                });

                if (cur.length > longest.length) {
                    active = true;
                } else {
                    active = false;
                }

                /*if(idx != -1) {
                    if(idxCount == undefined) {

                        idxCount = idx

                        cur.push({
                            date: e.date,
                            game: e.g
                        })

                    }else{
                        if(idx == idxCount + 1) {
                            // streak still going
                            // ++cur
                            cur.push({
                                date: e.date,
                                game: e.g
                            })
                        }else{

                            if(cur.length > longest.length) {
                                longest = cur.slice(0)
                            }

                            cur.length = 0

                            cur.push({
                                date: e.date,
                                game: e.g
                            })
                        }
                    }
                }*/
            } else {
                if (cur.length > longest.length) {
                    longest = cur.slice(0);
                }

                cur.length = 0;
                active = false;
            }
        });

        if (cur.length > longest.length) {
            longest = cur.slice(0);
        }

        resolve({ streak: longest, length: longest.length, active });
    });
}

module.exports = {
    Entry,
    entrySchema,

    create: (newEntry, callback) => {
        // Game.findById(newEntry.g, (err, game) => {

        //     if(err || !game) {
        //         return callback.error(err)
        //     }

        // newEntry.date = g.date

        Entry.create(newEntry, (err, entry) => {
            if (err) {
                return callback.error(err);
            } else if (!entry) {
                return callback.error('No document');
            }

            if (!Player.setToActive) {
                Player = require('../player/index');
            }

            Player.setToActive(entry.p);

            callback.success(entry);
        });

        // })
    },

    update: (entryId, entryData, callback) => {
        delete entryData.g;
        delete entryData.p;

        Entry.findByIdAndUpdate(entryId, entryData, { new: true }, (err, entry) => {
            if (err) {
                return callback.error(err);
            } else if (!entry) {
                return callback.error(`Entry not found.`);
            }

            callback.success(entry);
        });
    },

    delete: (entryId, callback) => {
        Entry.findByIdAndRemove(entryId, (err, res) => {
            if (err) {
                return callback.error(err);
            }
            let resObj = res ? { deleted: true } : { notFound: true };
            callback.success(resObj);
        });
    },

    readAll: (callback) => {
        const q = {};
        findEntries(q, callback);
    },

    readAllByGame: (gameId, callback) => {
        const q = {
            g: gameId,
        };
        findEntries(q, callback);
    },

    readAllByPlayer: (playerId, callback) => {
        const q = {
            p: playerId,
        };
        findEntries(q, callback);
    },

    getTotalEntries: (playerId, gameIds) => {
        const q = {
            p: playerId,
            g: { $in: gameIds },
        };

        if (!playerId) {
            delete q.p;
        }

        return Entry.count(q);
    },

    getTotalPoints: (playerId, gameIds) => {
        const q = {
            p: playerId,
            pt: true,
            g: { $in: gameIds },
            // q: 1
        };

        if (!playerId) {
            delete q.p;
        }

        return Entry.count(q);
    },

    getTotalBuyins: (playerId, gameIds) => {
        const q = {
            p: mongoose.Types.ObjectId(playerId),
            g: { $in: gameIds },
            // q: 1
        };

        if (!playerId) {
            delete q.p;
        }

        return new Promise((resolve, reject) => {
            Entry.aggregate([
                {
                    $match: q,
                },
                {
                    $group: {
                        _id: null,
                        buyins: { $sum: '$b' },
                    },
                },
            ]).exec((err, entries) => {
                if (err) {
                    return reject(err);
                }

                // resolve(entries[0] ? entries[0].buyins * 20 : 0)
                resolve(entries[0] ? entries[0].buyins : 0);
            });
        });
    },

    getTotalHighHandEntries: (playerId, gameIds) => {
        const q = {
            p: mongoose.Types.ObjectId(playerId),
            g: { $in: gameIds },
            q: 1,
            hh: 1,
        };

        if (!playerId) {
            delete q.p;
        }

        return Entry.count(q);
    },

    getLongestStreak: (playerId, gameIds) => {
        return new Promise((resolve, reject) => {
            const q = {
                p: mongoose.Types.ObjectId(playerId),
                g: { $in: gameIds },
                q: 1,
            };

            Entry.find(q)
                .select('g date pt')
                .sort('date')
                .exec((err, entries) => {
                    if (err) {
                        return reject(err);
                    }

                    getPlayerLongestStreak(entries)
                        .then((data) => {
                            resolve(data);
                        })
                        .catch((err) => {
                            reject(err);
                        });
                });
        });
    },

    /*getLongestStreakAverage : (gameIds) => {

        return new Promise((resolve, reject) => {

            const q = {
                g: {$in: gameIds},
                q: 1
            }
    
            Entry.aggregate([
                {
                    $match: q
                },{
                    $group: {
                        _id: '$p',
                        games: {'$push' : '$g'},
                        dates: {'$push' : '$date'},
                        points: {'$push' : '$pt'}
                    }
                }], (err, data) => {

                    if(err) {
                        return reject(err)
                    }

                    const ps = []
                    const all = []

                    data.forEach(d => {

                        let player = []

                        d.games.forEach((g, i) => {
                            player.push({
                                _id: d._id,
                                g: g,
                                date: d.dates[i],
                                pt: d.points[i]
                            })
                        })

                        player = player.sort((a, b) => {
                            return a.date - b.date
                        })

                        ps.push(player)
                    })

                    ps.forEach(p => {
                        all.push(getPlayerLongestStreak(p))
                    })

                    Promise.all(all)
                        .then(streaksData => {

                            let streakTotal = 0

                            streaksData.forEach(s => {
                                streakTotal += s.length
                            })

                            resolve(streakTotal / streaksData.length)
                        })
                        .catch(err => {
                            console.log(err)
                            reject(err)
                        })
                })
        })
    },*/

    getHighHandWinnings: (playerId, highHands) => {
        // return new Promise((resolve, reject) => {
        // console.log(playerId)
        // console.log(highHands)
        // resolve({cool: true})

        const allGames = highHands.map((h) => h.g._id);
        const q = {
            g: { $in: allGames },
            hh: 1,
        };

        return Entry.count(q);

        // Entry.aggregate([
        //     {
        //         $match: q
        //     },{
        //         $group: {
        //             _id: null,
        //             buyins: {$sum: '$b'}
        //         }
        //     }
        // ], (err, entries) => {

        //     if(err) {
        //         return reject(err)
        //     }

        //     resolve(entries[0].buyins * 20)
        // })
        // })
    },

    getTotalPotMoneyWon: (playerId, gameIds) => {
        const q = {
            p: mongoose.Types.ObjectId(playerId),
            g: { $in: gameIds },
            // q: 1
        };

        if (!playerId) {
            delete q.p;
        }

        return new Promise((resolve, reject) => {
            Entry.aggregate([
                {
                    $match: q,
                },
                {
                    $group: {
                        _id: null,
                        won: { $sum: '$c' },
                    },
                },
            ]).exec((err, entries) => {
                if (err) {
                    return reject(err);
                }

                resolve(entries[0] ? entries[0].won : 0);
            });
        });
    },

    readEntry: (entryId, callback) => {
        Entry.findById(entryId)
            .populate('p', 'first last')
            .populate('g', 'd m y')
            .exec((err, entry) => {
                if (err) {
                    return callback.error(err);
                }

                callback.success(entry);
            });
    },

    getPlayersFromGames: (gameIds) => {
        const q = { g: { $in: gameIds } };

        return Entry.find(q).select('p');
    },

    getUniquePlayers: (ids) => {
        const q = {
            g: { $in: ids },
        };

        return Entry.distinct('p', q);
    },

    getTotalThirdPlaceFinishes: (playerId, gameIds) => {
        return new Promise((resolve, reject) => {
            const q = {
                p: playerId,
                g: { $in: gameIds },
                c: { $gt: 0 },
                pt: false,
            };

            Entry.count(q).exec((err, count) => {
                if (err) {
                    return reject(err);
                }
                resolve(count);
            });
        });
    },

    getTotalRebuysForYear: (playerId, gameIds) => {
        return new Promise((resolve, reject) => {
            const q = {
                p: mongoose.Types.ObjectId(playerId),
                g: { $in: gameIds },
            };

            Entry.aggregate([
                {
                    $match: q,
                },
                {
                    $group: {
                        _id: '$g',
                        count: { $sum: '$b' },
                    },
                },
            ]).exec((err, buyins) => {
                if (err) {
                    return reject(err);
                } else if (!buyins || !buyins.length) {
                    return resolve(-1);
                }
                const totalRebuys = buyins.reduce((a, b) => {
                    return b.count - 1 + a;
                }, 0);
                resolve(totalRebuys);
            });
        });
    },

    getTotalGamesWithoutRebuy: (playerId, gameIds) => {
        // return new Promise((resolve, reject) => {
        const q = {
            p: mongoose.Types.ObjectId(playerId),
            g: { $in: gameIds },
            b: 1,
        };

        return Entry.count(q);
        // });
    },

    getTotalWinsWithoutRebuy: (playerId, gameIds) => {
        // return new Promise((resolve, reject) => {
        const q = {
            p: mongoose.Types.ObjectId(playerId),
            g: { $in: gameIds },
            b: 1,
            pt: true,
        };

        return Entry.count(q);
        // });
    },

    readBiggestCashByPlayer: (playerId, gameIds) => {
        return new Promise((resolve, reject) => {
            const q = {
                p: playerId,
                g: { $in: gameIds },
                c: { $gt: 0 },
            };

            Entry.find(q)
                .select('c')
                .sort('-c')
                .limit(1)
                .exec((err, doc) => {
                    if (err) {
                        return reject(err);
                    }

                    const highestCash = doc.length ? doc[0].c : 0;

                    resolve({ highestCash });
                });
        });
    },

    getEntriesFromGames: (ids) => {
        return new Promise((resolve, reject) => {
            const q = {
                g: { $in: ids },
            };

            Entry.find(q)
                .select('g hh')
                .exec((err, docs) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(docs);
                });
        });
    },

    getWinnersForGameIds: (ids) => {
        return new Promise((resolve, reject) => {
            const q = {
                g: { $in: ids },
            };

            Entry.aggregate([
                {
                    $match: q,
                },
                {
                    $group: {
                        _id: '$g',
                        ps: { $push: '$p' },
                        pts: { $push: '$pt' },
                        bs: { $push: '$b' },
                        cs: { $push: '$c' },
                    },
                },
            ]).exec((err, docs) => {
                if (err) {
                    return reject(err);
                }

                const retDocs = docs.map((d) => {
                    const ret = {
                        game: d._id,
                        winners: [],
                    };
                    const entries = [];

                    d.ps.forEach((p, i) => {
                        const pt = d.pts[i];
                        const b = d.bs[i];
                        const c = d.cs[i];

                        entries.push({ p, pt, b, c });
                    });

                    ret.winners = entries.filter((e) => e.pt);

                    return ret;
                });
                const retObj = {};

                retDocs.forEach((g) => {
                    retObj[g.game] = g.winners;
                });

                resolve(retObj);
            });
        });
    },
};

/*
if (process.env.NODE_ENV != 'copy') {
    console.log('COPY THAT SHIT - entry.js');

    requestify
        .request(`http://localhost:4321/api/entries`, {
            method: 'GET',
            dataType: 'json',
        })
        .then((response) => {
            const old = JSON.parse(response.body);
            // console.log(old);

            console.log('found', old.length);

            const all = old.map((e) => {
                const copy = { ...e };

                delete copy.__v;

                return Entry.findByIdAndUpdate(e._id, copy, { upsert: true });
            });

            Promise.all(all)
                .then((resp) => {
                    console.log('done!', resp.length);
                })
                .catch((err) => {
                    console.log({ err });
                });
        })
        .catch((err) => {
            console.log({ err });
        });
}
*/
