let io

function emitType(type, obj) {
    io.sockets.emit(type, obj)
}

exports.init = (server, users) => {

    io = require('socket.io')(server)

    io.on('connection', socket => {

        let token = socket.handshake.query.token

        users[token] = true

        socket.on('message', obj => {
            emitType('message', obj)
        })

        // socket.emit('users', users)
        // io.sockets.emit('users', users)
        emitType('users', users)

        socket.on('disconnect', () => {
    
            // let idx = users.indexOf(socket.handshake.query.token)

            // if(idx != -1) {
            //     users.splice(idx, 1)
            // }

            if(users[socket.handshake.query.token]) {
                delete users[socket.handshake.query.token]
            }
            
            // io.sockets.emit('users', users)
            emitType('users', users)
        })

        console.log(`>>>>>> ${token} connected to the socket server`)
    })
}