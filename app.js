const app = require('express')();
const config = require('./misc');
const mongoose = require('mongoose');
const api = require('./api');
const path = require('path');

let db;
let dbCreds = {};
let users = {}; // socket connected users

config.getDbCreds(dbCreds, connectDatabase);

function connectDatabase() {
    const promise = mongoose.connect(config.dbUrl(dbCreds), {
        useMongoClient: true,
        server: {
            reconnectTries: 5,
            reconnectInterval: 1000,
        },
    });

    promise
        .then((db) => {
            fireItUp(db);
        })
        .catch((err) => {
            console.log('ERROR CONNECTING TO DATABASE!');
            console.log(err);
        });
}

function fireItUp(db) {
    console.log('>>> Database connection successful.');

    const server = app.listen(config.port, () => {
        console.log(`Server running - ${config.port}.`);

        config.applyMiddleware(app);
        api.registerRoutes(app);

        require('./socket').init(server, users);

        app.get('/.well-known/pki-validation/1B4ACE147061F497DEE7CC9498D7135B.txt', (req, res) => {
            return res.sendFile(path.resolve('../data/validation.txt'));
        });

        app.get('*', (req, res) => {
            return res.sendFile(path.resolve('../data/client/index.html'));
        });
    });
}
