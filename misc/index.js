module.exports = {
    port: process.env.PORT || 1234,

    applyMiddleware: require('./middleware'),

    getRandomDonkey: require('./donkeys'),

    constants: require('./constants'),

    dbUrl: (dbCreds) => {
        if (process.env.NODE_ENV == 'test') {
            return `mongodb://${dbCreds.user}:${dbCreds.pass}@ds157980.mlab.com:57980/theravenhurst`;
        } else if (process.env.NODE_ENV == 'copy') {
            return `mongodb://${dbCreds.user}:${dbCreds.pass}@ds119585.mlab.com:19585/theravenhurst2`;
        }
        // return `mongodb://${dbCreds.user}:${dbCreds.pass}@theravenhurst.ubw85.mongodb.com/theravenhurst2?retryWrites=true`;
        return `mongodb://${dbCreds.user}:${dbCreds.pass}@blqxautfsjjldfy-mongodb.services.clever-cloud.com:27017/blqxautfsjjldfy`;
        // return `mongodb://${dbCreds.user}:${dbCreds.pass}@theravenhurst.ubw85.mongodb.net/theravenhurst2`;
    },

    getDbCreds: (dbCreds, dbCredsRead) => {
        return require('./getDbCreds')(dbCreds, dbCredsRead);
    },

    auth: require('./auth'),

    renameIndex: (index_renamed) => {
        return require('./renameIndex')(index_renamed);
    },

    routerCallback: (res) => {
        return {
            error: (err) => {
                // console.log('ERROR', err)
                // const retErr = typeof err == 'string' ? err : JSON.stringify(err)
                // res.status(400).send({ reason : retErr })
                res.status(400).send({ reason: err });
            },

            success: (doc) => {
                res.send(doc);
            },
        };
    },
};
