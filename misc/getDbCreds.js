const jsonfile = require('jsonfile')
const file = './misc/localConfig.json'

module.exports = (dbCreds, cb) => {

    if(process.env.DB_USER && process.env.DB_PASS) {
        
        dbCreds.user = process.env.DB_USER
        dbCreds.pass = process.env.DB_PASS
        
        return cb()
    }

    jsonfile.readFile(file, (err, obj) => {

        if(err) {
            return console.log('No database creds in env or local file!')
        }
            
        dbCreds.user = obj.user
        dbCreds.pass = obj.pass

        cb()
    })
}