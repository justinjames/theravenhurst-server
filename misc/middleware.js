const cookieParser = require('cookie-parser');
const passport = require('passport');
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
// const favicon = require('serve-favicon')
const express = require('express');
const logger = require('morgan');
const path = require('path');
const fs = require('fs');
const methodOverride = require('method-override');
const errorhandler = require('errorhandler');
const publicPath = path.normalize(__dirname + '/../../data/client');
const donkeyPath = path.normalize(__dirname + '/../../data/donkeys');
const storagePath = path.normalize(__dirname + '/../../data/storage');
const smackImagesPath = path.normalize(__dirname + '/../../data/smackImages');
const favPath = path.normalize(__dirname + '/../../data/client/assets/icon');

module.exports = app => {
	app.use(bodyParser.json({ limit: '50mb' }));
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(cookieParser('onlyAtTheRaven'));
	app.use('/data/donkeys', express.static(donkeyPath));
	app.use('/data/storage', express.static(storagePath));
	app.use('/data/smackImages', express.static(smackImagesPath));
	app.use(express.static(publicPath));

	// if (fs.existsSync(favPath)) {
	//     app.use(favicon(favPath))
	// }

	app.use(
		session({
			secret: 'justintotalbadass',
			key: 'danlovesdiets',
			store: new MongoStore({
				mongooseConnection: mongoose.connection,
				ssl: true,
				ttl: 30 * 24 * 60 * 60,
			}),
			resave: true,
			saveUninitialized: true,
		})
	);
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(methodOverride('X-HTTP-Method-Override'));
	app.use(errorhandler());
	app.use((req, res, next) => {
		res.header('Access-Control-Allow-Origin', '*');
		res.header(
			'Access-Control-Allow-Headers',
			'Origin, X-Requested-With, Content-Type, Accept'
		);
		next();
	});
	app.use(logger('dev'));
};
