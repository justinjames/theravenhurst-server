const fs = require('fs')
const donkeyDir = '../data/donkeys'
let donkeyFiles

fs.readdir(donkeyDir, (err, files) => {
    if (err) {
        return console.log(err)
    }
    donkeyFiles = files
})

module.exports = () => {
    if(donkeyFiles && donkeyFiles.length > 0) {
        const rand = Math.floor(Math.random() * donkeyFiles.length)
        return '/data/donkeys/' + donkeyFiles[rand]
    }else{
        return null
    }
}