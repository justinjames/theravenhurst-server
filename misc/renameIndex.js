const path = require('path')
const fs = require('fs')
const indexpath = path.normalize(__dirname + '/../data/client/index.html')
const indexnewpath = path.normalize(__dirname + '/../data/client/index-template.html')

module.exports = callback => {
    // check if index.html exists
    fs.stat(indexpath, (err, exists) => {
        
        if (exists) {
            
            // index.html exists.  check if index-template.html exists

            fs.stat(indexnewpath, (err, exs) => {
                
                if(exs) {
                    // they should never both exists ... 
                    callback()
                }else{
                    // does not exist .... rename the file
                    fs.rename(indexpath, indexnewpath, () => {
                        callback()
                    })
                }

            })
            
        }else{
            callback()
        }
    })
}