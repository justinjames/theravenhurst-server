const passport = require('passport')
const mongoose = require('mongoose')
const Player = require('../api/player/model').Player
const LocalStrategy = require('passport-local').Strategy

function passesLocal(headers) {
    if(headers && headers.istwixonmydesk && headers.istwixonmydesk == 'tw1x1s1nthem1x') {
        return true
    }
    return false
}

passport.use(new LocalStrategy((email, password, done) => {
    
    Player.findOne({ email })
        .exec((err, player) => {
            if(player && player.auth(password)) {
                done(null, player)
            }else{
                done(null, false)
            }
        })
}))

passport.serializeUser((player, done) => {
    if(player) {
        done(null, player._id)
    }
})

passport.deserializeUser((_id, done) => {
    Player.findOne({ _id })
        .exec(function(err, player) {
            if(player) {
                return done(null, player)
            }else{
                return done(null, false)
            }
        })
})

module.exports = {

    authenticate : (req, res, next) => {

        req.logout()
        req.body.email = req.body.email.toLowerCase()
        req.body.username = req.body.email

        const auth = passport.authenticate('local', (err, player) => {

            if(err) {
                return next(err)
            }else if(!player) {
                return res.status(400).send({ reason: 'Invalid password or player not found.' })
            }

            req.logIn(player, err => {
                if(err) { return next(err) }
                player.login = new Date()
                player.save()
                return res.send({ success : true, player })
            })
        })

        auth(req, res, next)
    },

    logout : (req, res, next) => {
        req.logout()
        res.end()
    },

    isLoggedIn : (req, res, next) => {
        if(passesLocal(req.headers)) {
            return next()
        }
        if(req.isAuthenticated()) {
            return next()
        }
        res.status(403).end()
    },

    isAdmin : (req, res, next) => {
        if(!req.isAuthenticated() || !req.user || !req.user.admin) {
            res.status(403).end()
        }else{
            next()
        }
    },

    isSamePlayerOrAdmin : (req, res, next) => { // or is admin
        // return (req, res, next) => {
            if(req.isAuthenticated() && ((req.user && req.user._id == req.params.playerId) || req.user.admin)) {
                return next()
            }
            res.status(403).end()
        // }
    },

    isSamePlayer : (req, res, next) => {
        if(req.isAuthenticated() && req.user && req.user._id == req.params.playerId) {
            return next()
        }
        res.status(403).end()
    }
}